NFS is a wearable tool that delivers additional security to any credit card holder. By creating a bluetooth field between the wallet and the smartwatch, it ensures that the user won't let his things behind. 

NFS warns the user when the wallet gets out of her reach, and has the capability to void its associated credit card if necessary. It easily helps to avoid distractions, and minimizes the cost of losing a card by immediately warning the user and cancelling the compromised card.

This wearable app works as a transparent background service, appearing only when it is necessary.