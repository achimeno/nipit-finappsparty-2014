package com.nipit.forcefield;

import android.app.Activity;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.wearable.view.WatchViewStub;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ForceFieldAlertActivity extends Activity {

    private TextView mTextView;
     private static Vibrator vibrator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        vibrator = (Vibrator) this.getSystemService(this.VIBRATOR_SERVICE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_force_field_alert);
        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                mTextView = (TextView) stub.findViewById(R.id.text);

                Button buttonuhoh = (Button)  stub.findViewById(R.id.buttonuhoh_id);
                buttonuhoh.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        cancelVibrator();

               /*         Intent confirmIntent = new Intent(getBaseContext(), ForceFieldDialogActivity.class);

                        confirmIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                        getBaseContext().startActivity(confirmIntent);
*/
                        setContentView(com.nipit.forcefield.R.layout.activity_force_field_dialog);
                        final WatchViewStub stub = (WatchViewStub) findViewById(com.nipit.forcefield.R.id.watch_view_stub_dialog);
                        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
                            @Override
                            public void onLayoutInflated(WatchViewStub stub) {
                                mTextView = (TextView) stub.findViewById(com.nipit.forcefield.R.id.text);

                                final Button buttonNevermind = (Button) stub.findViewById(com.nipit.forcefield.R.id.button_nevermind);
                                buttonNevermind.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View v) {

                                        System.exit(0);
                                    }
                                });

                                final Button buttonCancelAccount = (Button) stub.findViewById(com.nipit.forcefield.R.id.button_cancelAccount);
                                buttonCancelAccount.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View v) {
                                        Toast.makeText(getApplicationContext(), "Card Cancelled", Toast.LENGTH_SHORT).show();
                                        System.exit(0);
                                    }
                                });
                            }
                        });


                    }
                });

                Button buttondismiss = (Button)  stub.findViewById(R.id.buttondismiss_id);
                buttondismiss.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        cancelVibrator();
                        System.exit(0);
                    }
                });
            }
        });





        long[] pattern = {0, 100, 100, 100, 100, 400, 200};

        vibrator.vibrate(pattern, 0);



    }

    public void cancelVibrator(){
        vibrator.cancel();
    }
}
