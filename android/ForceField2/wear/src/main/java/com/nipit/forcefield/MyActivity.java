package com.nipit.forcefield;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.wearable.view.WatchViewStub;
import android.widget.TextView;

public class MyActivity extends Activity {

    private TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.nipit.forcefield.R.layout.activity_my);

        Intent serviceIntent = new Intent(this, HelloService.class);
        this.startService(serviceIntent);

        final WatchViewStub stub = (WatchViewStub) findViewById(com.nipit.forcefield.R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                mTextView = (TextView) stub.findViewById(com.nipit.forcefield.R.id.text);

                TextView mTextView2 = (TextView) stub.findViewById(com.nipit.forcefield.R.id.textView2);
                mTextView2.setTypeface(null, Typeface.ITALIC);
            }
        });




    }
}
