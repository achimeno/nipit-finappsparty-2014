package com.nipit.forcefield;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;
import android.os.Process;

import com.nipit.forcefield.ForceFieldAlertActivity;

import java.util.Set;

public class HelloService extends Service {
  private Looper mServiceLooper;
  private ServiceHandler mServiceHandler;
  private BluetoothAdapter bluetoothAdapter;


    // Handler that receives messages from the thread
  private final class ServiceHandler extends Handler {
      public ServiceHandler(Looper looper) {
          super(looper);
      }
      @Override
      public void handleMessage(Message msg) {



          final BluetoothManager bluetoothManager =
                  (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
          bluetoothAdapter = bluetoothManager.getAdapter();

          Set<BluetoothDevice> devices = bluetoothAdapter.getBondedDevices();




          for (BluetoothDevice device:devices){
              device.connectGatt(getBaseContext(),true,mGattCallback);
          }



          // Normally we would do some work here, like download a file.
          // For our sample, we just sleep for 5 seconds.
          long endTime = System.currentTimeMillis() + 300*1000;
          while (System.currentTimeMillis() < endTime) {
              synchronized (this) {
                  try {
                      boolean raiseAlarm = false;

                      Log.d("HALOOO","HEIII");
                      wait(5 * 1000);




/*                      final BluetoothManager bluetoothManager =
                              (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
                      bluetoothAdapter = bluetoothManager.getAdapter();

                      Set<BluetoothDevice> devices = bluetoothAdapter.getBondedDevices();




                      for (BluetoothDevice device:devices){

                        try {

                            Method method = device.getClass().getMethod("getUuids"); /// get all services
                            ParcelUuid[] parcelUuids = (ParcelUuid[]) method.invoke(device); /// get all services



                            BluetoothSocket bS = device.createInsecureRfcommSocketToServiceRecord(parcelUuids[0].getUuid());
                            bS.connect();
                            bS.close();
                        } catch (Exception e){
                            raiseAlarm = true;
                        }

                         int state = bluetoothManager.getConnectionState(device, BluetoothProfile.GATT);
                          Log.d("state",""+state);
                          if (state == BluetoothProfile.STATE_CONNECTED){
                              Log.d("state","STATE_CONNECTED");

                         }
                          if (state == BluetoothProfile.STATE_CONNECTING){
                              Log.d("state","STATE_CONNECTING");
                          }
                          if (state == BluetoothProfile.STATE_DISCONNECTED){
                              Log.d("state","STATE_DISCONNECTED");
                          }
                          if (state == BluetoothProfile.STATE_DISCONNECTING){
                              Log.d("state","STATE_DISCONNECTING");
                          }
                      }



                      Log.d("bonded devices:","bonded devices: "+devices.size());
                      if (raiseAlarm){
                          //TODO: Launch An activity with all the shit from losing your wallet...
                          Intent alertIntent = new Intent(getBaseContext(), ForceFieldAlertActivity.class);

                          alertIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                       //  getBaseContext().startActivity(alertIntent);

                      }
*/
                  } catch (Exception e) {
                      Log.d("Exception",e.getStackTrace().toString());
                  }
              }
          }









          Log.d("Service", "Service End");
          // Stop the service using the startId, so that we don't stop
          // the service in the middle of handling another job
          stopSelf(msg.arg1);
      }
  }


    private final BluetoothGattCallback mGattCallback =
            new BluetoothGattCallback() {
                @Override
                public void onConnectionStateChange(BluetoothGatt gatt, int status,
                                                    int newState) {
                    Log.d("STATE","onConnectionStateChange");
                    String intentAction;
                    if (newState == BluetoothProfile.STATE_CONNECTED) {
                        Log.d("STATE","STATE_CONNECTED");

                    } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                        Log.d("STATE","STATE_DISCONNECTED");
                        Intent alertIntent = new Intent(getBaseContext(), ForceFieldAlertActivity.class);

                        alertIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                        getBaseContext().startActivity(alertIntent);
                    } else if (newState == BluetoothProfile.STATE_CONNECTING) {
                        Log.d("STATE","STATE_CONNECTING");

                    } else if (newState == BluetoothProfile.STATE_DISCONNECTING) {
                        Log.d("STATE","STATE_DISCONNECTING");

                    }
                }
            };

    @Override
  public void onCreate() {
    // Start up the thread running the service.  Note that we create a
    // separate thread because the service normally runs in the process's
    // main thread, which we don't want to block.  We also make it
    // background priority so CPU-intensive work will not disrupt our UI.
    HandlerThread thread = new HandlerThread("ServiceStartArguments",
            Process.THREAD_PRIORITY_BACKGROUND);
    thread.start();
      Log.d("Service", "Service Start");
    // Get the HandlerThread's Looper and use it for our Handler








      mServiceLooper = thread.getLooper();
    mServiceHandler = new ServiceHandler(mServiceLooper);






  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
      Toast.makeText(this, "service starting", Toast.LENGTH_SHORT).show();


      // Connect bluetooth










      // For each start request, send a message to start a job and deliver the
      // start ID so we know which request we're stopping when we finish the job
      Message msg = mServiceHandler.obtainMessage();
      msg.arg1 = startId;
      mServiceHandler.sendMessage(msg);

      // If we get killed, after returning from here, restart
      return START_STICKY;
  }

  @Override
  public IBinder onBind(Intent intent) {
      // We don't provide binding, so return null
      return null;
  }

  @Override
  public void onDestroy() {
    Toast.makeText(this, "service done", Toast.LENGTH_SHORT).show();
  }
}