package com.nipit.forcefield;

import android.app.Activity;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothProfile;
import android.content.Intent;
import android.os.Bundle;
import android.support.wearable.view.WatchViewStub;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ForceFieldDialogActivity extends Activity {

    private TextView mTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.nipit.forcefield.R.layout.activity_force_field_dialog);
        final WatchViewStub stub = (WatchViewStub) findViewById(com.nipit.forcefield.R.id.watch_view_stub_dialog);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                mTextView = (TextView) stub.findViewById(com.nipit.forcefield.R.id.text);

                final Button buttonNevermind = (Button) stub.findViewById(com.nipit.forcefield.R.id.button_nevermind);
                buttonNevermind.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        System.exit(0);
                    }
                });

                final Button buttonCancelAccount = (Button) stub.findViewById(com.nipit.forcefield.R.id.button_cancelAccount);
                buttonCancelAccount.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        Toast.makeText(getApplicationContext(), "Card Cancelled", Toast.LENGTH_SHORT).show();
                        System.exit(0);
                    }
                });
            }
        });




    }

    private final BluetoothGattCallback mGattCallback =
            new BluetoothGattCallback() {
                @Override
                public void onConnectionStateChange(BluetoothGatt gatt, int status,
                                                    int newState) {
                    Log.d("STATE", "onConnectionStateChange");
                    String intentAction;
                    if (newState == BluetoothProfile.STATE_CONNECTED) {
                        Log.d("STATE","STATE_CONNECTED");

                    } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                        Log.d("STATE","STATE_DISCONNECTED");
                        Intent alertIntent = new Intent(getBaseContext(), ForceFieldAlertActivity.class);

                        alertIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                        getBaseContext().startActivity(alertIntent);
                    } else if (newState == BluetoothProfile.STATE_CONNECTING) {
                        Log.d("STATE","STATE_CONNECTING");

                    } else if (newState == BluetoothProfile.STATE_DISCONNECTING) {
                        Log.d("STATE","STATE_DISCONNECTING");

                    }
                }
            };

}
